#!/bin/bash

# Copyright 2017, Raging Goblin <https://raginggoblin.wordpress.com/>
# 
# This file is part of MiWakeUpLight.
# 
#  MiWakeUpLight is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
# 
#  MiWakeUpLight is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
# 
#  You should have received a copy of the GNU General Public License
#  along with MiWakeUpLight.  If not, see <http://www.gnu.org/licenses/>.
# 


echo "Create user miwakeuplight"
useradd -M -N -r miwakeuplight

echo "Install software"
mkdir -p /opt/miwakeuplight
cp target/miwakeuplight.jar /opt/miwakeuplight/miwakeuplight.jar
cp etc/application.sample.properties /opt/miwakeuplight/application.properties
chown -R miwakeuplight /opt/miwakeuplight
touch /var/log/miwakeuplight.log
chown miwakeuplight /var/log/miwakeuplight.log

echo "Configure service"
cp etc/miwakeuplight.service /etc/systemd/system/miwakeuplight.service
chmod +x /etc/systemd/system/miwakeuplight.service
systemctl enable miwakeuplight.service

echo "Start service"
systemctl restart miwakeuplight.service
