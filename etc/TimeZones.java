/*
 * Copyright 2017, Raging Goblin <https://raginggoblin.wordpress.com/>
 * 
 * This file is part of MiWakeUpLight.
 *
 *  MiWakeUpLight is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  MiWakeUpLight is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with MiWakeUpLight.  If not, see <http://www.gnu.org/licenses/>.
 */

import java.util.TimeZone;

public class TimeZones {

   public static void main(String[] args) {

      System.out.println("====================   Available Time Zones   ====================");
      
      for (String id : TimeZone.getAvailableIDs()) {
         if (args.length == 0 || id.toLowerCase().contains(args[0].toLowerCase())) {
            System.out.println(id);
         }
      }
      
      System.out.println("====================   Current Time Zone   ====================");
      System.out.println(TimeZone.getDefault().getID());
   }
}