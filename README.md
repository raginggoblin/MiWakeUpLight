# MiWakeUpLight

A wakeuplight for a Mi Light Dual White lamp with Spring Boot and Angularjs.   
See https://raginggoblin.wordpress.com/category/miwakuplight/ for more information. 

<p align="center"><img src="https://gitlab.com/raginggoblin/MiWakeUpLight/raw/master/images/AllSchedulesBootstrap.png" /></p>

# 1. Usage
This software can be used to create a wakeup light with a Mi Light Dual White lamp. This software should be run on a server which can be a simple single board device like a Raspberry Pi. The software will then send commands to a Mi Light wifi controller starting a sunrise when you want to wake up.

<p align="center"><img src="https://gitlab.com/raginggoblin/MiWakeUpLight/raw/master/images/SingleScheduleBootstrap.png" /></p>

For each schedule you can control the following settings:
* Time: This is the time the lamp will shine with its maximum brightness.
* Duration: This is the duration of the wakeup cycle, i.e. the time between the first step (10%) and the last step (100%).
* Daylight length: The lamp will shine at a 100% after reaching full strength for this amount of time.
* Days active: The days of the week the current schedule will be active.

Note, that it is not advisable to configure 2 overlapping schedules as these schedules will both send their commands causing the lamp to perform a disco sunrise.

There are some buttons in the navigation bar to control the lamp directly. You can turn it on, off, change the brightness and color. This might interfere with your wakeup schedules;

# 2. Installing the software
The software can run on any operating system where you have Java 8 installed, but it is recommended to install it on a Debian system or a Linux system using systemd, e.g. a Raspberry Pi using raspbian will do. 

<p align="center"><img src="https://gitlab.com/raginggoblin/MiWakeUpLight/raw/master/images/Diagram.png" /></p>

#### 2.1. Download and unzip software
Download the latest release from [https://gitlab.com/raginggoblin/MiWakeUpLight/-/jobs/74380654/artifacts/download](https://gitlab.com/raginggoblin/MiWakeUpLight/-/jobs/74380654/artifacts/download) and unzip it. I will refer with `<unzip>` to the folder where you unzipped this archive.
#### 2.2. Create a configuration file
Inside the zip file you will find a configuration file `<unzip>/etc/application.sample.properties`. At least configure the IP address of your wifi controller. You should also check your current timezone. By running the command `java TimeZones` inside the folder `<unzip>/etc` you can view the current timezone and the available timezones. You can filter the output by providing an extra parameter, e.g. `java TimeZones Europe` will only show the available timezones in Europe. If your timezone is not correct configure the correct value in `<unzip>/etc/application.sample.properties`. The file contains other configurations as well, so read the comments.
#### 2.3. Run the installer script
**As root user** run the installer script: `<unzip>/install.sh` i.e. `./install.sh`.

Now you can browse to `http://\<ip address of your server\>:8080` and configure your wakeup schedules.

If you want to change one of the configurations afterwards, you will find you configuration in 
`/opt/miwakeuplight/application.properties`. 

# 3. Deinstall the software
**As root user** run the uninstall script: `<unzip>/uninstall.sh` i.e. `./uninstall.sh`