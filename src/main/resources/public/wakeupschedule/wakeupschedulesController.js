/*
 * Copyright 2017, Raging Goblin <https://raginggoblin.wordpress.com/>
 * 
 * This file is part of MiWakeUpLight.
 *
 *  MiWakeUpLight is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  MiWakeUpLight is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with MiWakeUpLight.  If not, see <http://www.gnu.org/licenses/>.
 */

(function() {

	angular.module("miwakeuplight").controller("wakeupschedulesController", wakeupschedulesController);

	wakeupschedulesController.$inject = [ "$http", "$location", "$interval", "$timeout", "$scope", "$rootScope",
			"CONSTANTS" ];

	function wakeupschedulesController($http, $location, $interval, $timeout, $scope, $rootScope, CONSTANTS) {

		var vm = this;

		vm.wakeupSchedules = [];
		vm.firstWakeup = null;

		$rootScope.$on("error", function(event, message) {
			vm.errorMessage = message;
			$timeout(function() {
				vm.errorMessage = undefined;
			}, 4000);
		});

		vm.toSchedule = function(id) {
			$location.path(id);
		}

		vm.switchOn = function() {
			vm.directControl("on");
		}

		vm.switchOff = function() {
			vm.directControl("off");
		}
		
		vm.turnUp = function() {
			vm.directControl("up");
		}
		
		vm.turnDown = function() {
			vm.directControl("down");
		}
		
		vm.turnWarmer = function() {
			vm.directControl("warmer");
		}
		
		vm.turnCooler = function() {
			vm.directControl("cooler");
		}

		vm.gotoDirectControl = function() {
            $location.path("directcontrol");
		}

		vm.directControl = function(action) {
			$http({
				method : "GET",
				url : "api/control/" + action
			})
		}

		vm.createNewSchedule = function() {
			var newSchedule = {
				"active" : true,
				"time" : 25200000,
				"duration" : 1800000,
				"dayLength" : 3600000,
				"daysActive" : CONSTANTS.DAYS.slice()
			}
			$http({
				method : "POST",
				url : "api/wakeupschedules/",
				params : {
					wakeUpSchedule : newSchedule
				}
			}).then(function(response) {
				$location.path(response.data.id);
			});
		}
		
		vm.toggleActive = function(schedule) {
			$http({
				method : "POST",
				url : "api/wakeupschedules/",
				params : {
					wakeUpSchedule : schedule
				}
			}).then(function(response) {
				vm.refreshData();
			});
		}

		vm.deleteSchedule = function(id) {
			$http({
				method : "DELETE",
				url : "api/wakeupschedules/" + id
			}).then(function(response) {
				vm.refreshData();
			});
		}

		vm.areSchedulesOverlapping = function() {
			for (var i = 0; i < vm.wakeupSchedules.length; i++) {
				if (vm.wakeupSchedules[i].overlap) {
					return true;
				}
			}
			return false;
		}

		vm.setOverlap = function() {
			for (var i = 0; i < vm.wakeupSchedules.length; i++) {
				var leftSchedule = vm.wakeupSchedules[i];
				for (var j = 0; j < vm.wakeupSchedules.length; j++) {
					var rightSchedule = vm.wakeupSchedules[j];
					if (leftSchedule != rightSchedule && !leftSchedule.overlap) {
						leftSchedule.overlap = vm.hasOverlap(leftSchedule, rightSchedule);
					}
				}
			}
		}

		vm.hasOverlap = function(leftSchedule, rightSchedule) {
		    if(leftSchedule.active && rightSchedule.active) {
                if (vm.hasEqualElement(leftSchedule.daysActive, rightSchedule.daysActive)
                    || vm.hasEqualElement(rightSchedule.daysActive, leftSchedule.daysActive)) {

                    return leftSchedule.time + leftSchedule.dayLength >= rightSchedule.time - rightSchedule.duration
                        && leftSchedule.time - leftSchedule.duration <= rightSchedule.time + rightSchedule.dayLength;
                }
            }
			return false;
		}

		vm.hasEqualElement = function(leftArray, rightArray) {
			for (var i = 0; i < leftArray.length; i++) {
				for (var j = 0; j < rightArray.length; j++) {
					if (leftArray[i] == rightArray[j]) {
						return true;
					}
				}
			}
			return false;
		};

		vm.refreshData = function() {
			$http({
				method : "GET",
				url : "api/wakeupschedules",
			}).then(function(response) {
				vm.wakeupSchedules = response.data;
				vm.setOverlap();
				vm.refreshFirstWakeup();
			});
		}
		
		vm.refreshFirstWakeup = function() {
			$http({
				method : "GET",
				url : "api/wakeupschedules/first",
			}).then(function(response) {
				vm.firstWakeup = response.data;
			});
		}

		vm.refreshData();
		vm.interval = $interval(vm.refreshData, 10000);
		$scope.$on("$destroy", function() {
			$interval.cancel(vm.interval);
			vm.interval = undefined;
		});
	}

}());