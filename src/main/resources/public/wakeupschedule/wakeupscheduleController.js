/*
 * Copyright 2017, Raging Goblin <https://raginggoblin.wordpress.com/>
 * 
 * This file is part of MiWakeUpLight.
 *
 *  MiWakeUpLight is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  MiWakeUpLight is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with MiWakeUpLight.  If not, see <http://www.gnu.org/licenses/>.
 */

(function() {

	angular.module("miwakeuplight").controller("wakeupscheduleController", wakeupscheduleController);

	wakeupscheduleController.$inject = [ "$http", "$routeParams", "$location", "$log", "$scope", "$rootScope", "$timeout", "CONSTANTS" ];

	function wakeupscheduleController($http, $routeParams, $location, $log, $scope, $rootScope, $timeout, CONSTANTS) {

		var vm = this;

		vm.id = $routeParams.id;
		vm.wakeupSchedule;
		vm.time;
		vm.duration;
		vm.dayLength;
		vm.days = [];
		
		$rootScope.$on("error", function(event, message) {
			vm.errorMessage = message;
			$timeout(function() {
				vm.errorMessage = undefined;
			}, 4000);
		});

		vm.toSchedule = function(id) {
			$location.path(id);
		}

		vm.goHome = function() {
			$location.path("/");
		}

		vm.watchTime = function() {
			$scope.$watch(angular.bind(vm, function() {
				return vm.time;
			}), function(newValue, oldValue) {
				if (newValue.getTime() != oldValue.getTime()) {
					vm.saveData();
				}
			})
		};

		vm.refreshData = function() {
			$http({
				method : "GET",
				url : "api/wakeupschedules/" + vm.id,
			}).then(function(response) {
				vm.wakeupSchedule = response.data;

				var time = new Date(vm.wakeupSchedule.time);
				var utc = time.getTime() + (time.getTimezoneOffset() * 60000);
				vm.time = new Date(utc);

				vm.duration = vm.wakeupSchedule.duration / 60000;
				vm.dayLength = vm.wakeupSchedule.dayLength / 60000;

				for (var i = 0; i < vm.wakeupSchedule.daysActive.length; i++) {
					for (var j = 0; j < vm.days.length; j++) {
						if (vm.days[j].value == vm.wakeupSchedule.daysActive[i]) {
							vm.days[j].active = true;
						}
					}
				}

				vm.watchTime();
			});
		}

		vm.saveData = function() {
			var utc = vm.time.getTime() - (vm.time.getTimezoneOffset() * 60000);
			vm.wakeupSchedule.time = utc
			vm.wakeupSchedule.duration = 60000 * vm.duration;
			vm.wakeupSchedule.dayLength = 60000 * vm.dayLength;
			var daysActive = [];
			for (var j = 0; j < vm.days.length; j++) {
				if (vm.days[j].active) {
					daysActive.push(vm.days[j].value);
				}
			}
			vm.wakeupSchedule.daysActive = daysActive;
			$http({
				method : "POST",
				url : "api/wakeupschedules/",
				params : {
					wakeUpSchedule : vm.wakeupSchedule
				}
			}).then(function(response) {
				$log.info("wakeupschedule saved: " + JSON.stringify(response.data));
			});
		}

		for (var i = 0; i < CONSTANTS.DAYS.length; i++) {
			var day = CONSTANTS.DAYS[i];
			var label = day.charAt(0).toUpperCase() + day.slice(1).toLowerCase();
			vm.days.push({
				active : false,
				label : label,
				value : day
			});
		}

		vm.refreshData();
	}

}());