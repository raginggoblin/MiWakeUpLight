/*
 * Copyright 2017, Raging Goblin <https://raginggoblin.wordpress.com/>
 * 
 * This file is part of MiWakeUpLight.
 *
 *  MiWakeUpLight is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  MiWakeUpLight is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with MiWakeUpLight.  If not, see <http://www.gnu.org/licenses/>.
 */

(function() {

	angular.module("miwakeuplight", [ "ngRoute", "ui.bootstrap"]).config(
			function($routeProvider, $httpProvider) {

				$routeProvider
                .when("/404", {
					templateUrl : "404.html"
				}).when("/", {
					templateUrl : "wakeupschedule/wakeupschedules.html",
					controller : "wakeupschedulesController",
					controllerAs : "controller"
				}).when("/directcontrol", {
					templateUrl : "directcontrol/directcontrol.html",
					controller : "directcontrolController",
					controllerAs : "controller"
				}).when("/:id", {
					templateUrl : "wakeupschedule/wakeupschedule.html",
					controller : "wakeupscheduleController",
					controllerAs : "controller"
				}).otherwise({
					redirectTo : "/404"
				});

				$httpProvider.interceptors.push("errorInterceptor");
			});

	angular.module("miwakeuplight").factory("errorInterceptor", function($q, $log, $rootScope) {
		return {
			"response" : function(response) {
				return response;
			},
			"responseError" : function(error) {
				$log.error("Something went wrong", error);
				$rootScope.$emit("error", "Error communicating with the server, please refresh the page or contact your system administrator.");
				return $q.reject(error);
			}
		};
	});

	angular.module("miwakeuplight").filter(
			"daysformatter",
			function() {
				return function(daysArray) {
					if (daysArray.indexOf("MONDAY") > -1 && daysArray.indexOf("TUESDAY") > -1
							&& daysArray.indexOf("WEDNESDAY") > -1 && daysArray.indexOf("THURSDAY") > -1
							&& daysArray.indexOf("FRIDAY") > -1) {
						if (daysArray.indexOf("SATURDAY") > -1 && daysArray.indexOf("SUNDAY") > -1) {
							return "Every day";
						}
						if (daysArray.indexOf("SATURDAY") == -1 && daysArray.indexOf("SUNDAY") == -1) {
							return "Mon - Fri";
						}
					}

					daysArray = daysArray.map(function(day) {
						return day.charAt(0).toUpperCase() + day.slice(1, 3).toLowerCase();
					});
					return daysArray.join(", ");
				}
			});

	angular.module("miwakeuplight").constant("CONSTANTS", {
		DAYS : [ "MONDAY", "TUESDAY", "WEDNESDAY", "THURSDAY", "FRIDAY", "SATURDAY", "SUNDAY" ]
	});

}());