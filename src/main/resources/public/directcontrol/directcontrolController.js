/*
 * Copyright 2020, Raging Goblin <https://raginggoblin.wordpress.com/>
 * 
 * This file is part of MiWakeUpLight.
 *
 *  MiWakeUpLight is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  MiWakeUpLight is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with MiWakeUpLight.  If not, see <http://www.gnu.org/licenses/>.
 */

(function() {

	angular.module("miwakeuplight").controller("directcontrolController", directcontrolController);

    directcontrolController.$inject = [ "$http", "$timeout", "$rootScope" ];

	function directcontrolController($http, $timeout, $rootScope) {

		var vm = this;

		$rootScope.$on("error", function(event, message) {
			vm.errorMessage = message;
			$timeout(function() {
				vm.errorMessage = undefined;
			}, 4000);
		});

        vm.up = function() {
            vm.directControl("up");
        }

        vm.down = function() {
            vm.directControl("down");
        }

        vm.cooler = function() {
            vm.directControl("cooler");
        }

        vm.warmer = function() {
            vm.directControl("warmer");
        }

        vm.directControl = function(action) {
            $http({
                method : "GET",
                url : "api/control/" + action
            })
        }
	}

}());