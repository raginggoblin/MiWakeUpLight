/*
 * Copyright 2017, Raging Goblin <https://raginggoblin.wordpress.com/>
 *
 * This file is part of MiWakeUpLight.
 *
 *  MiWakeUpLight is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  MiWakeUpLight is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with MiWakeUpLight.  If not, see <http://www.gnu.org/licenses/>.
 */

package raging.goblin.miwakeuplight.service;

import java.io.IOException;
import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ScheduledFuture;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.util.Pair;
import org.springframework.scheduling.TaskScheduler;
import org.springframework.stereotype.Service;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import raging.goblin.miwakeuplight.WifiControllerAPI;
import raging.goblin.miwakeuplight.domain.WakeUpSchedule;
import raging.goblin.miwakeuplight.storage.WakeUpScheduleRepository;

@Service
@Slf4j
public class WakeUpService {

   @Value("${raging.goblin.miwakeuplight.wificontroller.light}")
   private int wificontrollerLight;

   @Autowired
   private TaskScheduler scheduler;
   @Autowired
   private WakeUpScheduleRepository wakeUpScheduleRepository;
   @Autowired
   private WifiControllerAPI wifiController;

   @Getter
   private Pair<Date, Date> firstWakeup;

   private Map<Long, ScheduledSchedule> scheduledWakeUps = new HashMap<>();

   @PostConstruct
   public void scheduleAllWakeUps() {
      List<WakeUpSchedule> allSchedules = wakeUpScheduleRepository.findAll();
      allSchedules.forEach(s -> scheduleSingleWakeUp(s));
      updateFirstWakeupTime();
   }

   public void scheduleDeleted(Long id) {
      ScheduledSchedule scheduledWakeup = scheduledWakeUps.get(id);
      if (scheduledWakeup != null) {
         synchronized (scheduler) {
            scheduledWakeup.getFuture().cancel(true);
         }

         log.info("Schedule with id {} canceled", id);

         if (isWakeupAlreadyRunning(scheduledWakeup)) {
            new Thread(() -> switchLightOff()).start();
            log.info("Schedule was already running, so we switched the light off");
         }
      }
      updateFirstWakeupTime();
   }

   public void scheduleSaved(WakeUpSchedule savedWakeUpSchedule) {
      scheduleDeleted(savedWakeUpSchedule.getId());
      scheduleSingleWakeUp(savedWakeUpSchedule);
      updateFirstWakeupTime();
   }

   public Map<Date, WakeUpSchedule> getScheduledSchedules() {
      return scheduledWakeUps.values().stream()
              .sorted((s1, s2) -> s1.scheduledDateTime.compareTo(s2.scheduledDateTime))
              .collect(Collectors.toMap(s -> s.scheduledDateTime, s -> s.schedule));
   }

   private void switchLightOff() {
      try {
         log.debug("Set brightness to 0%");
         wifiController.brightness(0, wificontrollerLight);
         Thread.sleep(10000);
         wifiController.off(wificontrollerLight);
         log.debug("Switched light off");
      } catch (InterruptedException | IllegalArgumentException | IOException e) {
         log.error("Unable to switch off light properly", e);
      }
   }

   private boolean isWakeupAlreadyRunning(ScheduledSchedule scheduledWakeup) {
      return Math.abs(scheduledWakeup.getScheduledDateTime().getTime() - new Date().getTime()) < scheduledWakeup
            .getSchedule().getDuration() + scheduledWakeup.getSchedule().getDayLength();
   }

   private void scheduleSingleWakeUp(WakeUpSchedule schedule) {
      if (schedule.isActive() && !schedule.getDaysActive().isEmpty()) {
         int timeBetweenRunners = schedule.getDuration() / 10;
         BrightnessRunner runner = new BrightnessRunner(0, timeBetweenRunners, schedule);
         Date startTime = calculateNextRun(schedule);
         ScheduledFuture<?> runnerFuture = scheduler.schedule(runner, startTime);
         scheduledWakeUps.put(schedule.getId(), new ScheduledSchedule(schedule, runnerFuture, startTime));
         log.info("Wakeup scheduled: {} at {}", schedule, new Date(startTime.getTime() + schedule.getDuration()));
      }
   }

   private Date calculateNextRun(WakeUpSchedule schedule) {
      ZonedDateTime scheduledTime = ZonedDateTime.now().truncatedTo(ChronoUnit.DAYS);
      scheduledTime = scheduledTime.plus(schedule.getTime() - schedule.getDuration(), ChronoUnit.MILLIS);
      if (scheduledTime.isBefore(ZonedDateTime.now())) {
         scheduledTime = scheduledTime.plus(1, ChronoUnit.DAYS);
      }

      while (!schedule.getDaysActive().contains(scheduledTime.getDayOfWeek())) {
         scheduledTime = scheduledTime.plus(1, ChronoUnit.DAYS);
      }

      return Date.from(scheduledTime.toInstant());
   }

   private void updateFirstWakeupTime() {
      firstWakeup = null;
      List<WakeUpSchedule> allSchedules = wakeUpScheduleRepository.findAll();
      allSchedules.forEach(s -> {
         if(s.isActive()) {
            Date nextRun = calculateNextRun(s);
            if(firstWakeup == null || nextRun.before(firstWakeup.getFirst())) {
               Date endTime = new Date(nextRun.getTime() + s.getDuration());
               firstWakeup = Pair.of(nextRun, endTime);
            }
         }
      });
   }

   @AllArgsConstructor
   private class BrightnessRunner implements Runnable {

      private int brightness;
      private int timeBetweenRunners;
      private WakeUpSchedule schedule;

      @Override
      public void run() {
         try {
            log.debug("Set brightness to {}%", brightness);
            wifiController.brightness(brightness, wificontrollerLight);
         } catch (IllegalArgumentException | IOException e) {
            log.error("Unable to set brightness to " + brightness, e);
         }

         if (brightness < 100) {
            scheduleNextBrightness();
         } else if (brightness == 100) {
            scheduleDayLightEnds();
         }
      }

      private void scheduleNextBrightness() {
         BrightnessRunner newRunner = new BrightnessRunner(brightness + 10, timeBetweenRunners, schedule);
         Date startTime = new Date(new Date().getTime() + timeBetweenRunners);

         synchronized (scheduler) {
            ScheduledFuture<?> runnerFuture = scheduler.schedule(newRunner, startTime);
            scheduledWakeUps.put(schedule.getId(), new ScheduledSchedule(schedule, runnerFuture, startTime));
         }

         updateFirstWakeupTime();

         log.debug("Scheduled next brightness step for {}", startTime);
      }

      private void scheduleDayLightEnds() {
         Date dayEndTime = new Date(new Date().getTime() + schedule.getDayLength());
         Runnable dayLightEndsRunner = new DayLightEndsRunner(schedule);

         synchronized (scheduler) {
            ScheduledFuture<?> runnerFuture = scheduler.schedule(dayLightEndsRunner, dayEndTime);
            scheduledWakeUps.put(schedule.getId(), new ScheduledSchedule(schedule, runnerFuture, dayEndTime));
         }

         updateFirstWakeupTime();

         log.debug("Scheduled daylight end for {}", dayEndTime);
      }
   }

   @AllArgsConstructor
   private class DayLightEndsRunner implements Runnable {

      private WakeUpSchedule schedule;

      @Override
      public void run() {
         switchLightOff();
         scheduleSingleWakeUp(schedule);
      }
   }

   @Data
   @AllArgsConstructor
   private class ScheduledSchedule {

      private WakeUpSchedule schedule;
      private ScheduledFuture<?> future;
      private Date scheduledDateTime;
   }
}
