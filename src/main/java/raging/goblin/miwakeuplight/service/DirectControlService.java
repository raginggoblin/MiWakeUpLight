/*
 * Copyright 2018, Raging Goblin <https://raginggoblin.wordpress.com/>
 *
 * This file is part of MiWakeUpLight.
 *
 *  MiWakeUpLight is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  MiWakeUpLight is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with MiWakeUpLight.  If not, see <http://www.gnu.org/licenses/>.
 */

package raging.goblin.miwakeuplight.service;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import lombok.extern.slf4j.Slf4j;
import raging.goblin.miwakeuplight.WifiControllerAPI;

@Slf4j
@Service
public class DirectControlService {

   @Value("${raging.goblin.miwakeuplight.wificontroller.light}")
   private int wificontrollerLight;

   @Autowired
   private WifiControllerAPI wifiController;

   public void switchOn() {
      try {
         wifiController.on(wificontrollerLight);
      } catch (IllegalArgumentException | IOException e) {
         log.error("Unable to switch light on", e);
      }
   }

   public void switchOff() {
      new Thread(() -> {
         try {
            wifiController.brightness(0, wificontrollerLight);
            Thread.sleep(2000);
            wifiController.off(wificontrollerLight);
         } catch (IllegalArgumentException | IOException | InterruptedException e) {
            log.error("Unable to switch light off", e);
         }
      }).start();
   }

   public void turnUp() {
      try {
         int currentBrightness = wifiController.getCurrentBrightnessPercentage();
         currentBrightness += 10;
         currentBrightness = Math.min(WifiControllerAPI.MAX_BRIGHTNESS - 1, currentBrightness);
         wifiController.brightness(currentBrightness, wificontrollerLight);
      } catch (IllegalArgumentException | IOException e) {
         log.error("Unable to turn brighness up", e);
      }
   }

   public void turnDown() {
      try {
         int currentBrightness = wifiController.getCurrentBrightnessPercentage();
         currentBrightness -= 10;
         currentBrightness = Math.max(0, currentBrightness);
         wifiController.brightness(currentBrightness, wificontrollerLight);
      } catch (IllegalArgumentException | IOException e) {
         log.error("Unable to turn brighness down", e);
      }
   }

   public void turnWarmer() {
      try {
         int currentWarmness = wifiController.getCurrentWarmnessPercentage();
         currentWarmness += 10;
         currentWarmness = Math.min(WifiControllerAPI.MAX_BRIGHTNESS - 1, currentWarmness);
         wifiController.warmness(currentWarmness, wificontrollerLight);
      } catch (IllegalArgumentException | IOException e) {
         log.error("Unable to turn warmer", e);
      }
   }

   public void turnCooler() {
      try {
         int currentWarmness = wifiController.getCurrentWarmnessPercentage();
         currentWarmness -= 10;
         currentWarmness = Math.max(0, currentWarmness);
         wifiController.warmness(currentWarmness, wificontrollerLight);
      } catch (IllegalArgumentException | IOException e) {
         log.error("Unable to turn cooler", e);
      }
   }
}
