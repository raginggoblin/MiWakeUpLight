/*
 * Copyright 2017, Raging Goblin <https://raginggoblin.wordpress.com/>
 *
 * This file is part of MiWakeUpLight.
 *
 *  MiWakeUpLight is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  MiWakeUpLight is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with MiWakeUpLight.  If not, see <http://www.gnu.org/licenses/>.
 */

package raging.goblin.miwakeuplight;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.util.ArrayList;
import java.util.List;

import lombok.extern.slf4j.Slf4j;

/**
 * This class represents a MiLight WiFi box (v4) and is able to send commands to a particular box.
 */
@Slf4j
public class WifiControllerAPI {

   /**
    * The address of the WiFi box
    */
   private InetAddress address;

   /**
    * The port of the WiFi box
    */
   private int port;

   /**
    * The current brightness level.
    */
   private Integer brightness;

   /**
    * The current warmness level.
    */
   private Integer warmness;

   /**
    * The default port for unconfigured boxes.
    */
   public static final int DEFAULT_PORT = 8899;

   /**
    * The sleep time between both messages for switching lights to the white mode.
    */
   public static final int DEFAULT_SLEEP_BETWEEN_MESSAGES = 100;

   /**
    * The maximum value for the brightness.
    */
   public static final int MAX_BRIGHTNESS = 101;

   /**
    * The maximum value for the warmness.
    */
   public static final int MAX_WARMNESS = 101;

   /**
    * The command code for "WHITE LED ALL OFF".
    */
   public static final int COMMAND_ALL_OFF = 0x39;

   /**
    * The command code for "GROUP 1 ALL OFF".
    */
   public static final int COMMAND_GROUP_1_OFF = 0x3B;

   /**
    * The command code for "GROUP 2 ALL OFF".
    */
   public static final int COMMAND_GROUP_2_OFF = 0x33;

   /**
    * The command code for "GROUP 3 ALL OFF".
    */
   public static final int COMMAND_GROUP_3_OFF = 0x3A;

   /**
    * The command code for "GROUP 4 ALL OFF".
    */
   public static final int COMMAND_GROUP_4_OFF = 0x36;

   /**
    * The command code for "WHITE LED ALL ON".
    */
   public static final int COMMAND_ALL_ON = 0x35;

   /**
    * The command code for "GROUP 1 ALL ON".
    */
   public static final int COMMAND_GROUP_1_ON = 0x38;

   /**
    * The command code for "GROUP 2 ALL ON".
    */
   public static final int COMMAND_GROUP_2_ON = 0x3D;

   /**
    * The command code for "GROUP 3 ALL ON".
    */
   public static final int COMMAND_GROUP_3_ON = 0x37;

   /**
    * The command code for "GROUP 4 ALL ON".
    */
   public static final int COMMAND_GROUP_4_ON = 32;

   /**
    * The command code for "SET COLOR TO WHITE (GROUP ALL)". Send an "ON" command 100ms before.
    */
   public static final int COMMAND_ALL_WHITE = 0xB5;

   /**
    * The command code for "SET COLOR TO WHITE (GROUP 1)". Send an "ON" command 100ms before.
    */
   public static final int COMMAND_GROUP_1_WHITE = 0xB8;

   /**
    * The command code for "SET COLOR TO WHITE (GROUP 2)". Send an "ON" command 100ms before.
    */
   public static final int COMMAND_GROUP_2_WHITE = 0xBD;

   /**
    * The command code for "SET COLOR TO WHITE (GROUP 3)". Send an "ON" command 100ms before.
    */
   public static final int COMMAND_GROUP_3_WHITE = 0xB7;

   /**
    * The command code for "SET COLOR TO WHITE (GROUP 4)". Send an "ON" command 100ms before.
    */
   public static final int COMMAND_GROUP_4_WHITE = 0xB2;

   /**
    * The command code for "SET LED TO NIGHT (GROUP ALL)". Send an "OFF" command 100ms before.
    */
   public static final int COMMAND_ALL_NIGHT = 0xB9;

   /**
    * The command code for "SET LED TO NIGHT (GROUP 1)". Send an "OFF" command 100ms before.
    */
   public static final int COMMAND_GROUP_1_NIGHT = 0xBB;

   /**
    * The command code for "SET LED TO NIGHT (GROUP 2)". Send an "OFF" command 100ms before.
    */
   public static final int COMMAND_GROUP_2_NIGHT = 0xB3;

   /**
    * The command code for "SET LED TO NIGHT (GROUP 3)". Send an "OFF" command 100ms before.
    */
   public static final int COMMAND_GROUP_3_NIGHT = 0xBA;

   /**
    * The command code for "SET LED TO NIGHT (GROUP 4)". Send an "OFF" command 100ms before.
    */
   public static final int COMMAND_GROUP_4_NIGHT = 0xB6;

   /**
    * The command code for "BRIGHTER SETTING"
    */
   public static final int COMMAND_BRIGHTER = 0x3C;

   /**
    * The command code for "DARKER SETTING"
    */
   public static final int COMMAND_DARKER = 0x34;

   /**
    * The command code for "WARMER SETTING"
    */
   public static final int COMMAND_WARMER = 0x3E;

   /**
    * The command code for "COOLER SETTING"
    */
   public static final int COMMAND_COOLER = 0x3F;

   /**
    * A constructor creating a new instance of the WiFi box class.
    *
    * @param address
    *           is the address of the WiFi box
    * @param port
    *           is the port of the WiFi box (omit this if unsure)
    */
   public WifiControllerAPI(InetAddress address, int port) {
      this.address = address;
      this.port = port;
   }

   /**
    * This function sends an array of bytes to the WiFi box. The bytes should be a valid command, i.e. the array's
    * length should be three.
    *
    * @param messages
    *           is an array of message codes to send
    * @throws IllegalArgumentException
    *            if the length of the array is not 3
    * @throws IOException
    *            if the message could not be sent
    */
   private void sendMessage(byte[] messages) throws IOException {
      if (messages.length != 3) {
         throw new IllegalArgumentException("The message to send should consist of exactly 3 bytes.");
      }

      DatagramSocket socket = new DatagramSocket();
      DatagramPacket packet = new DatagramPacket(messages, messages.length, address, port);
      socket.send(packet);
      socket.close();
   }

   /**
    * This function pads a one-byte message to a three-byte message by adding the default bytes 0x00 0x55.
    *
    * @param message
    *           is the message to pad
    * @return is the padded message
    */
   private byte[] padMessage(int message) {
      byte[] paddedMessage = { (byte) message, 0x55 & 0x00, 0x55 & 0x55 };
      return paddedMessage;
   }

   /**
    * This function sends an one-byte control message to the WiFi box. The message is padded with 0x00 0x55 as given in
    * the documentation.
    *
    * @param message
    *           is the message code to send
    * @throws IOException
    *            if the message could not be sent
    */
   private void sendMessage(int message) throws IOException {
      // pad the message with 0x00 0x55
      byte[] paddedMessage = padMessage(message);

      // send the padded message
      sendMessage(paddedMessage);
   }

   /**
    * Creates a message to turn the light on.
    *
    * @param group
    *           the group to turn on (between 1 and 4, 0 for all)
    */
   private byte[] onMessage(int group) {
      switch (group) {
      case 0:
         return padMessage(COMMAND_ALL_ON);
      case 1:
         return padMessage(COMMAND_GROUP_1_ON);
      case 2:
         return padMessage(COMMAND_GROUP_2_ON);
      case 3:
         return padMessage(COMMAND_GROUP_3_ON);
      case 4:
         return padMessage(COMMAND_GROUP_4_ON);
      default:
         throw new IllegalArgumentException("The group number must be between 0 and 4");
      }
   }

   /**
    * Creates an array containing 10 messages to put the led in the darkest mode.
    *
    * @param group
    *           the group to set in darkest mode (between 1 and 4, 0 for all)
    */
   private byte[][] darkest(int group) {
      // create message array
      byte[][] messages = new byte[11][3];

      messages[0] = onMessage(group);

      for (int i = 1; i <= 10; i++) {
         messages[i] = padMessage(COMMAND_DARKER);
      }
      return messages;
   }

   /**
    * Creates an array containing 10 messages to put the led in the coolest mode.
    *
    * @param group
    *           the group to set in coolest mode (between 1 and 4, 0 for all)
    */
   private byte[][] coolest(int group) {
      // create message array
      byte[][] messages = new byte[11][3];

      switch (group) {
      case 0:
         messages[0] = padMessage(COMMAND_ALL_ON);
         break;
      case 1:
         messages[0] = padMessage(COMMAND_GROUP_1_ON);
         break;
      case 2:
         messages[0] = padMessage(COMMAND_GROUP_2_ON);
         break;
      case 3:
         messages[0] = padMessage(COMMAND_GROUP_3_ON);
         break;
      case 4:
         messages[0] = padMessage(COMMAND_GROUP_4_ON);
         break;
      default:
         throw new IllegalArgumentException("The group number must be between 0 and 4");
      }

      for (int i = 1; i <= 10; i++) {
         messages[i] = padMessage(COMMAND_COOLER);
      }
      return messages;
   }

   /**
    * This function sends multiple three-byte messages to the WiFi box. All elements of the message array should be byte
    * arrays with three elements. Note that the messages are sent in a new thread. Therefore, you should not send other
    * commands directly after executing this one. Also, there are no exceptions when sending messages fails since they
    * occur in another thread.
    *
    * @param messages
    *           is the messages to send (in order)
    * @param sleep
    *           is the time to wait between two message in milliseconds
    * @throws IllegalArgumentException
    *            if some of the messages in the array don't consist of exactly three bytes
    */
   private void sendMultipleMessages(final byte[][] messages, final long sleep) throws IllegalArgumentException {
      // check arguments
      for (int i = 0; i < messages.length; i++) {
         if (messages[i].length != 3) {
            throw new IllegalArgumentException("All messages should consist of three bytes.");
         }
      }

      // start new thread
      new Thread(new Runnable() {
         @Override
         public void run() {
            try {
               for (byte[] message : messages) {
                  WifiControllerAPI.this.sendMessage(message);
                  Thread.sleep(sleep);
               }
            } catch (IOException e) {
               // if the message could not be sent
            } catch (InterruptedException e) {
               // if the thread could not sleep
            }
         }
      }).start();
   }

   /**
    * This function sends multiple one-byte messages to the WiFi box. All of the are padded with the corresponding
    * bytes. Note that the messages are sent in a new thread. Therefore, you should not send other commands directly
    * after executing this one. Also, there are no exceptions when sending messages fails since they occur in another
    * thread.
    *
    * @param messages
    *           is the messages to send (in order)
    * @param sleep
    *           is the time to wait between two message in milliseconds
    */
   private void sendMultipleMessages(final int[] messages, final long sleep) {
      // pad messages
      byte[][] paddedMessages = new byte[messages.length][3];
      for (int i = 0; i < messages.length; i++) {
         paddedMessages[i] = padMessage(messages[i]);
      }

      // send the padded messages
      sendMultipleMessages(paddedMessages, sleep);
   }

   /**
    * Switch all lights off.
    *
    * @param group
    *           the group to switch (between 1 and 4 for a particular group, 0 for all groups)
    * @throws IOException
    *            if the message could not be sent
    * @throws IllegalArgumentException
    *            if the group number is not between 0 and 4
    */
   public void off(int group) throws IOException, IllegalArgumentException {
      switch (group) {
      case 0:
         sendMessage(COMMAND_ALL_OFF);
         break;
      case 1:
         sendMessage(COMMAND_GROUP_1_OFF);
         break;
      case 2:
         sendMessage(COMMAND_GROUP_2_OFF);
         break;
      case 3:
         sendMessage(COMMAND_GROUP_3_OFF);
         break;
      case 4:
         sendMessage(COMMAND_GROUP_4_OFF);
         break;
      default:
         throw new IllegalArgumentException("The group number must be between 0 and 4");
      }
   }

   /**
    * Switch all lights on.
    *
    * @param group
    *           the group to switch (between 1 and 4 for a particular group, 0 for all groups)
    * @throws IOException
    *            if the message could not be sent
    * @throws IllegalArgumentException
    *            if the group number is not between 0 and 4
    */
   public void on(int group) throws IOException, IllegalArgumentException {
      switch (group) {
      case 0:
         sendMessage(COMMAND_ALL_ON);
         break;
      case 1:
         sendMessage(COMMAND_GROUP_1_ON);
         break;
      case 2:
         sendMessage(COMMAND_GROUP_2_ON);
         break;
      case 3:
         sendMessage(COMMAND_GROUP_3_ON);
         break;
      case 4:
         sendMessage(COMMAND_GROUP_4_ON);
         break;
      default:
         throw new IllegalArgumentException("The group number must be between 0 and 4");
      }
   }

   /**
    * Switch all lights to the white mode. Note that the messages are sent in a new thread. Therefore, you should not
    * send other commands directly after executing this one. Also, there are no exceptions when sending messages fails
    * since they occur in another thread.
    *
    * @param group
    *           the group to switch (between 1 and 4 for a particular group, 0 for all groups)
    * @throws IllegalArgumentException
    *            if the group number is not between 0 and 4
    */
   public void white(int group) throws IllegalArgumentException {
      // create message array
      int[] messages = new int[2];
      switch (group) {
      case 0:
         messages[0] = COMMAND_ALL_ON;
         messages[1] = COMMAND_ALL_WHITE;
         break;
      case 1:
         messages[0] = COMMAND_GROUP_1_ON;
         messages[1] = COMMAND_GROUP_1_WHITE;
         break;
      case 2:
         messages[0] = COMMAND_GROUP_2_ON;
         messages[1] = COMMAND_GROUP_2_WHITE;
         break;
      case 3:
         messages[0] = COMMAND_GROUP_3_ON;
         messages[1] = COMMAND_GROUP_3_WHITE;
         break;
      case 4:
         messages[0] = COMMAND_GROUP_4_ON;
         messages[1] = COMMAND_GROUP_4_WHITE;
         break;
      default:
         throw new IllegalArgumentException("The group number must be between 0 and 4");
      }

      // send messages
      sendMultipleMessages(messages, DEFAULT_SLEEP_BETWEEN_MESSAGES);
   }

   /**
    * Switch all lights to the night mode. Note that the messages are sent in a new thread. Therefore, you should not
    * send other commands directly after executing this one. Also, there are no exceptions when sending messages fails
    * since they occur in another thread.
    *
    * @param group
    *           the group to switch of (between 1 and 4 for a particular group, 0 for all groups)
    * @throws IllegalArgumentException
    *            if the group number is not between 0 and 4
    */
   public void night(int group) throws IllegalArgumentException {
      // create message array
      int[] messages = new int[2];
      switch (group) {
      case 0:
         messages[0] = COMMAND_ALL_OFF;
         messages[1] = COMMAND_ALL_NIGHT;
         break;
      case 1:
         messages[0] = COMMAND_GROUP_1_OFF;
         messages[1] = COMMAND_GROUP_1_NIGHT;
         break;
      case 2:
         messages[0] = COMMAND_GROUP_2_OFF;
         messages[1] = COMMAND_GROUP_2_NIGHT;
         break;
      case 3:
         messages[0] = COMMAND_GROUP_3_OFF;
         messages[1] = COMMAND_GROUP_3_NIGHT;
         break;
      case 4:
         messages[0] = COMMAND_GROUP_4_OFF;
         messages[1] = COMMAND_GROUP_4_NIGHT;
         break;
      default:
         throw new IllegalArgumentException("The group number must be between 0 and 4");
      }

      // send messages
      sendMultipleMessages(messages, DEFAULT_SLEEP_BETWEEN_MESSAGES);
   }

   /**
    * Resets the brightness value, sets it to null. This is necessary when someone outside the application has set the
    * brightness of the lamp. The stored brightness is not correct anymore.
    */
   public void resetBrightness() {
      brightness = null;
   }

   /**
    * Returns the current brightness on a scale between 0 and 100.
    */
   public int getCurrentBrightnessPercentage() {
      return brightness == null ? 0 : 10 * brightness;
   }

   /**
    * Set the brightness value for the provided group of lights.
    *
    * @param value
    *           is the brightness value to set (between 0 and MAX_BRIGHTNESS)
    * @param group
    *           the group to switch (between 1 and 4 for a particular group, 0 for all groups)
    * @throws IOException
    *            if the message could not be sent
    * @throws IllegalArgumentException
    *            if the brightness value is not between 0 and WiFiBox.MAX_BRIGHTNESS or the group not between 0 and 4.
    */
   public void brightness(int value, int group) throws IOException, IllegalArgumentException {
      // check argument
      if (value < 0 || value > MAX_BRIGHTNESS) {
         throw new IllegalArgumentException("The brightness value should be between 0 and WiFiController.MAX_BRIGHTNESS");
      }

      // lights have 10 levels of brightness
      value = (int) (Math.floor(value / 10.0));
      List<byte[]> messages = new ArrayList<>();

      // we do not know the current setting therefore we first go to the darkest level
      if (brightness == null) {
         byte[][] darkestMessages = darkest(group);
         for (int i = 0; i < darkestMessages.length; i++) {
            messages.add(darkestMessages[i]);
         }
         brightness = 0;
      } else {
         messages.add(onMessage(group));
      }

      int delta = value - brightness;
      brightness += delta;
      for (int j = 1; j <= Math.abs(delta); j++) {
         messages.add(padMessage(delta > 0 ? COMMAND_BRIGHTER : COMMAND_DARKER));
      }

      // send messages to the WiFi box
      sendMultipleMessages(messages.toArray(new byte[messages.size()][3]), DEFAULT_SLEEP_BETWEEN_MESSAGES);
      log.debug("Brightness is now {}", brightness);
   }

   /**
    * Returns the current warmness on a scale between 0 and 100.
    */
   public int getCurrentWarmnessPercentage() {
      return warmness == null ? 0 : 10 * warmness;
   }

   /**
    * Set the warmness value for the provided group of lights.
    *
    * @param value
    *           is the warmness value to set (between 0 and MAX_WARMNESS)
    * @param group
    *           the group to switch (between 1 and 4 for a particular group, 0 for all groups)
    * @throws IOException
    *            if the message could not be sent
    * @throws IllegalArgumentException
    *            if the warmness value is not between 0 and WiFiBox.MAX_WARMNESS or the group not between 0 and 4.
    */
   public void warmness(int value, int group) throws IOException, IllegalArgumentException {
      // check argument
      if (value < 0 || value > MAX_WARMNESS) {
         throw new IllegalArgumentException("The warmness value should be between 0 and WiFiBox.MAX_WARMNESS");
      }

      // lights have 10 levels of warmness
      value = (int) (Math.floor(value / 10.0));
      List<byte[]> messages = new ArrayList<>();

      // we do not know the current setting therefore we first go to the coolest level
      if (warmness == null) {
         byte[][] coolestMessages = coolest(group);
         for (int i = 0; i < coolestMessages.length; i++) {
            messages.add(coolestMessages[i]);
         }
         warmness = 0;
      }

      int delta = value - warmness;
      warmness += delta;
      for (int j = 0; j < Math.abs(delta); j++) {
         messages.add(padMessage(delta > 0 ? COMMAND_WARMER : COMMAND_COOLER));
      }

      // send messages to the WiFi box
      sendMultipleMessages(messages.toArray(new byte[messages.size()][3]), DEFAULT_SLEEP_BETWEEN_MESSAGES);
      log.debug("Warmness is now {}", warmness);
   }
}
