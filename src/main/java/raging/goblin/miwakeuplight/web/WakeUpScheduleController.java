/*
 * Copyright 2017, Raging Goblin <https://raginggoblin.wordpress.com/>
 *
 * This file is part of MiWakeUpLight.
 *
 *  MiWakeUpLight is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  MiWakeUpLight is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with MiWakeUpLight.  If not, see <http://www.gnu.org/licenses/>.
 */

package raging.goblin.miwakeuplight.web;

import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.util.Pair;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import lombok.extern.slf4j.Slf4j;
import raging.goblin.miwakeuplight.domain.WakeUpSchedule;
import raging.goblin.miwakeuplight.service.WakeUpService;
import raging.goblin.miwakeuplight.storage.WakeUpScheduleRepository;

@Slf4j
@RestController
@RequestMapping(value = "/api/wakeupschedules")
public class WakeUpScheduleController {

   @Autowired
   private WakeUpScheduleRepository wakeUpScheduleRepository;
   @Autowired
   private WakeUpService wakeUpService;

   @GetMapping("/first")
   public Pair<Date, Date> getFirstWakeupTime() {
      return wakeUpService.getFirstWakeup();
   }

   @GetMapping
   public List<WakeUpSchedule> getAll() {
      return wakeUpScheduleRepository.findAll();
   }

   @GetMapping("/{id}")
   public WakeUpSchedule getWakeUpSchedule(@PathVariable Long id) {
      return wakeUpScheduleRepository.findOne(id);
   }

   @GetMapping("/scheduled")
   public Map<Date, WakeUpSchedule> getScheduledSchedules() {
      return wakeUpService.getScheduledSchedules();
   }

   @DeleteMapping("/{id}")
   public void deleteWakeUpSchedule(@PathVariable Long id) {
      wakeUpScheduleRepository.delete(id);
      log.info("WakeUpSchedule with id {} deleted", id);

      wakeUpService.scheduleDeleted(id);
   }

   @PostMapping
   public WakeUpSchedule save(@RequestParam String wakeUpSchedule)
         throws JsonParseException, JsonMappingException, IOException {

      WakeUpSchedule wakeUpScheduleObject = new ObjectMapper().readValue(wakeUpSchedule, WakeUpSchedule.class);
      WakeUpSchedule savedWakeUpSchedule = wakeUpScheduleRepository.save(wakeUpScheduleObject);
      log.info("WakeUpSchedule saved: {}", savedWakeUpSchedule);

      wakeUpService.scheduleSaved(savedWakeUpSchedule);

      return savedWakeUpSchedule;
   }
}
