/*
 * Copyright 2018, Raging Goblin <https://raginggoblin.wordpress.com/>
 *
 * This file is part of MiWakeUpLight.
 *
 *  MiWakeUpLight is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  MiWakeUpLight is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with MiWakeUpLight.  If not, see <http://www.gnu.org/licenses/>.
 */

package raging.goblin.miwakeuplight.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import lombok.extern.slf4j.Slf4j;
import raging.goblin.miwakeuplight.service.DirectControlService;

@Slf4j
@RestController
@CrossOrigin
@RequestMapping(value = "/api/control")
public class DirectControlController {

   @Autowired
   private DirectControlService directControlService;

   @GetMapping("/on")
   public void switchOn() {
      log.debug("Direct control: switching on");
      directControlService.switchOn();
   }

   @GetMapping("/off")
   public void switchOff() {
      log.debug("Direct control: switching off");
      directControlService.switchOff();
   }

   @GetMapping("/up")
   public void turnUp() {
      log.debug("Direct control: turn up");
      directControlService.turnUp();
   }

   @GetMapping("/down")
   public void turnDown() {
      log.debug("Direct control: turn down");
      directControlService.turnDown();
   }

   @GetMapping("/warmer")
   public void turnWarmer() {
      log.debug("Direct control: turn warmer");
      directControlService.turnWarmer();
   }

   @GetMapping("/cooler")
   public void turnCooler() {
      log.debug("Direct control: turn cooler");
      directControlService.turnCooler();
   }
}
