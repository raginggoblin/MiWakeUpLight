/*
 * Copyright 2017, Raging Goblin <https://raginggoblin.wordpress.com/>
 * 
 * This file is part of MiWakeUpLight.
 *
 *  MiWakeUpLight is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  MiWakeUpLight is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with MiWakeUpLight.  If not, see <http://www.gnu.org/licenses/>.
 */

package raging.goblin.miwakeuplight;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.TimeZone;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.h2.server.web.WebServlet;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.TaskScheduler;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;
import org.springframework.util.StringUtils;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@SpringBootApplication
public class Application {

   @Value("${raging.goblin.miwakeuplight.wificontroller.host}")
   private String wificontrollerHost;
   @Value("${raging.goblin.miwakeuplight.wificontroller.port}")
   private int wificontrollerPort;

   private static ConfigurableApplicationContext applicationContext;

   public static void main(String[] args) {
      applicationContext = new SpringApplication(Application.class).run(args);
   }

   @Bean
   public TaskScheduler poolScheduler() {
      ThreadPoolTaskScheduler scheduler = new ThreadPoolTaskScheduler();
      return scheduler;
   }

   @Bean
   public WifiControllerAPI wifiControllerAPI() {
      try {
         return new WifiControllerAPI(InetAddress.getByName(wificontrollerHost), wificontrollerPort);
      } catch (UnknownHostException e) {
         log.error("Unable to find wificontroller host, it is of no use to go any further", e);
         applicationContext.close();
      }
      return null;
   }

   @Bean
   public ServletRegistrationBean h2servletRegistration() {
      ServletRegistrationBean registration = new ServletRegistrationBean(new WebServlet());
      registration.addUrlMappings("/console/*");
      return registration;
   }

   @Bean
   public ServletContextListener timeZoneInitializer() {

      return new ServletContextListener() {

         @Value("${raging.goblin.miwakeuplight.timezone:}")
         private String timezone;

         @Override
         public void contextInitialized(ServletContextEvent sce) {
            if (!StringUtils.isEmpty(timezone)) {
               TimeZone.setDefault(TimeZone.getTimeZone(timezone));
            }
         }

         @Override
         public void contextDestroyed(ServletContextEvent sce) {
            // Nothing to do
         }
      };
   }
}
