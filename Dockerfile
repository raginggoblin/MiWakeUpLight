FROM openjdk:8-alpine
EXPOSE 8080
WORKDIR /miwakeuplight
RUN addgroup -S miwakuplight && \
    adduser -S miwakuplight -G miwakuplight
COPY ./application.properties /miwakeuplight
COPY ./target/miwakeuplight.jar /miwakeuplight
RUN chown -R miwakuplight:miwakuplight /miwakeuplight
USER miwakuplight
CMD ["java", "-jar", "./miwakeuplight.jar"]
